open Data_types

let test_optf f = function
  | None -> None, true
  | Some x -> Some (f x), false
let test_opt x = test_optf (fun x -> x) x

let cal_to_str date = CalendarLib.Printer.Calendar.to_string date

let cal_of_str s =
  if s = "" then CalendarLib.Calendar.from_unixfloat 0.
  else CalendarLib.Printer.Calendar.from_string s

let pagination page page_size =
  let page = match page with None -> 0 | Some p -> p in
  let page_size = match page_size with None -> 20 | Some p -> p in
  Int64.of_int (page * page_size), Int64.of_int page_size

let donor_of_db (pkh, do_name, id, tsp, credit) =
  pkh, {
    do_name; do_id = Int64.to_int id; do_wus = 0; do_credit = Int64.to_int credit;
    do_last = CalendarLib.Printer.Calendar.to_string tsp}
