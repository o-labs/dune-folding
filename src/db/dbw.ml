open Data_types
open Metal_types
open Db_lwt
open MLet

let init_state ?tsp cycle =
  let>>> dbh = () in
  let> r = [%pgsql dbh "select last_cycle from state"] in
  match r with
  | [] ->
    let tsp = CalendarLib.Calendar.from_unixfloat 0. in
    [%pgsql dbh "insert into state(last_check, last_cycle) values($tsp, $cycle)"]
  | [ c ] -> [%pgsql dbh "update state set last_cycle = $cycle"]
  | _ -> Lwt.return_unit

let update_state cycle =
  let tsp = CalendarLib.Calendar.now () in
  let>>> dbh = () in
  let>+ () =
    [%pgsql dbh "update state set last_check = $tsp, last_cycle = $cycle"] in
  ()

let update_account pkh tsp credit cycle =
  let>>> dbh = () in
  [%pgsql dbh
      "update account set last_tsp = $tsp, last_credit = last_credit + $credit, \
       last_cycle = $cycle where pkh = $pkh"]

let register_account ?(cycle=0l) pkh {do_name; do_id; do_credit; do_last; _} =
  let id, credit = Int64.of_int do_id, Int64.of_int do_credit in
  let tsp = Misc_db.cal_of_str do_last in
  let>>> dbh = () in
  let>+ () = [%pgsql dbh
      "insert into account(pkh, folding_name, folding_id, last_tsp, last_credit, last_cycle)
       values($pkh, $do_name, $id, $tsp, $credit, $cycle)"] in ()

let register_distribution hash cycle ops total ratio =
  let>>> dbh = () in
  let> () = [%pgsql dbh
      "insert into distributions(hash, amount, cycle, ratio) \
       values($hash, $total, $cycle, $ratio)"] in
  Lwt_list.iter_s (function
      | {mo_det = TraDetails {trd_amount; trd_dst; _}; _} ->
        [%pgsql dbh
            "insert into transactions(hash, pkh, amount, cycle) \
             values($hash, $trd_dst, $trd_amount, $cycle)"]
      | _ -> Lwt.return_unit) ops
