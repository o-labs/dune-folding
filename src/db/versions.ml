
let downgrade_1_to_0 = []

let upgrade_0_to_1 dbh version =
  EzPG.upgrade ~dbh ~version ~downgrade:downgrade_1_to_0 [
    {|create table account(
      pkh varchar primary key,
      folding_name varchar not null,
      folding_id bigint unique not null,
      last_tsp timestamp not null,
      last_credit bigint not null)|};
    {|create table state(
      last_check timestamp not null)|}
  ]

let downgrade_2_to_1 = [
  {|alter table state drop column last_cycle|}
]

let upgrade_1_to_2 dbh version =
  EzPG.upgrade ~dbh ~version ~downgrade:downgrade_2_to_1 [
    {|alter table state add column last_cycle int not null default 0|};
  ]

let downgrade_3_to_2 = [
  {|alter table account drop column last_cycle|}
]

let upgrade_2_to_3 dbh version =
  EzPG.upgrade ~dbh ~version ~downgrade:downgrade_3_to_2 [
    {|alter table account add column last_cycle int not null default 0|};
    {|create table transactions(
      hash varchar not null,
      pkh varchar not null references account(pkh),
      amount bigint not null,
      cycle int not null)|};
    {|create table distributions(
      hash varchar not null,
      amount bigint not null,
      cycle int not null,
      ratio float not null)|}
  ]

let upgrades = [
  0, upgrade_0_to_1;
  1, upgrade_1_to_2;
  2, upgrade_2_to_3;
]

let downgrades = [
  3, downgrade_3_to_2;
  2, downgrade_2_to_1;
  1, downgrade_1_to_0
]
