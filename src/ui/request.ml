open Data_types
module S = Services

(** API Utils *)

let host = ref (EzAPI.TYPES.BASE "http://localhost:8080")

let wrap_res ?error f x = match x, error with
  | Ok x, _ -> f x
  | Error e, Some error -> let code, content = MRes.error_content e in
    error code content
  | _ -> ()

let get0 ?(host= !host) ?post ?headers ?params ?error ?(msg="") service f =
  EzXhr.get0 host service msg ?post ?headers ?error ?params (wrap_res ?error f) ()
let get1 ?(host= !host) ?post ?headers ?params ?error ?(msg="") service arg f =
  EzXhr.get1 host service msg ?post ?headers ?error ?params (wrap_res ?error f) arg
let post0 ?(host= !host) ?headers ?params ?error ?(msg="") ~input service f =
  EzXhr.post0 host service msg ?headers ?params ?error ~input (wrap_res ?error f)

(** Initialization of API server *)

let info_encoding = Json_encoding.(
    conv
      (fun {www_apis} -> www_apis)
      (fun www_apis -> {www_apis}) @@
    obj1
      (req "apis" (list string)))

let info_service : www_server_info Services.service0 =
  EzAPI.service
    ~output:info_encoding
    EzAPI.Path.(root // "info.json" )

let init f =
  get0 ~host:(Common.host ()) info_service
    ~error:(fun code content ->
        let s = match content with
          | None -> "network error"
          | Some content -> "network error: " ^ string_of_int code ^ " -> " ^ content in
        Common.logs s)
    (fun ({www_apis; _} as info) ->
       let api = List.nth www_apis (Random.int @@ List.length www_apis) in
       host := EzAPI.TYPES.BASE api;
       f info)

(** Requests *)

let register ?error re_pkh folding_id f =
  let re_folding = if folding_id = "" then re_pkh else folding_id in
  post0 ?error S.register_donor ~input:{re_pkh; re_folding} f

let get_info ?error id f =
  get1 ?error S.donor_info id f
