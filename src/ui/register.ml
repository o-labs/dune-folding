open Js_of_ocaml.Js

let register app =
  app##.registering := _true;
  let id =
    if app##.folding_id_ = string "" then app##.folding_name_
    else app##.folding_id_ in
  Request.register
    ~error:(fun code content ->
        app##.registering := _false;
        app##.registerError := Common.make_error code content)
    (to_string app##.pkh) (to_string id)
    (fun () ->
       app##.registering := _false;
       app##.registered := _true)

let get_info app =
  Request.get_info
    ~error:(fun code content -> app##.infoError := Common.make_error code content)
    (to_string app##.donor_id_)
    (fun (pkh, donor) -> app##.info := Common.js_donor pkh donor)

let init () =
  Vdata.add_method0 "register" register;
  Vdata.add_method0 "get_info" get_info
