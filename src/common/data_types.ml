type 'a api_result = ('a, MRes.error) result

type www_server_info = {
  www_apis : string list;
}

type donor = {
  do_name: string;
  do_id: int;
  do_wus: int;
  do_credit: int;
  do_last: string;
}

type register_input = {
  re_pkh: string;
  re_folding: string;
}
