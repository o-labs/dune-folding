open MLet

let cycle = ref None

let speclist = [
  "--cycle", Arg.Int (fun i -> cycle := Some (Int32.of_int i)), "Ask to print summary for a certain cycle"
]


let () =
  Arg.parse speclist (fun _ -> ()) "Distribution Printer";
  let r = Lwt_main.run @@
    let@! (hash, total, cycle, ratio), trs = Dbr.get_distribution ?cycle:!cycle () in
    Printf.printf "Distribution summary for cycle %ld :\n\n%!" cycle;
    let baking_reward = Int64.(of_float @@ (to_float total) /. ratio) in
    Printf.printf "Baking rewards : %Ld udun\n%!" baking_reward;
    Printf.printf "Distributed ratio : %f\n%!" ratio;
    Printf.printf "-> distributed reward : %Ld udun\n\n%!" total;
    Printf.printf "Batched transactions :\n%!";
    List.iteri (fun i (pkh, amount) ->
        Printf.printf "%d. Destination : %s\n   Amount : %Ld udun\n%!" (i+1) pkh amount) trs in
  match r with
  | Error e -> MRes.print_error e
  | Ok () -> ()
