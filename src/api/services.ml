open Data_types
open Encoding

let arg_string = EzAPI.arg_string "string" "string"
let param_team_name = EzAPI.Param.string "name"
let folding_host = EzAPI.TYPES.BASE "https://stats.foldingathome.org/api/"

type nonrec 'a service0 = ('a, MRes.error, EzAPI.no_security) EzAPI.service0
type nonrec ('a, 'b) service1 = ('a, 'b, MRes.error, EzAPI.no_security) EzAPI.service1
type nonrec ('a, 'b) post_service0 = ('a, 'b, MRes.error, EzAPI.no_security) EzAPI.post_service0

let error_outputs = [not_found; bad_request; server_error]

(** Folding API *)
let donor : (string, donor) service1 =
  EzAPI.service
    ~register:false
    ~output:donor_encoding
    ~error_outputs
    EzAPI.Path.(root // "donor" /: arg_string)

let team : (string, donor) service1 =
  EzAPI.service
    ~register:false
    ~output:donor_encoding
    ~error_outputs
    EzAPI.Path.(root // "team" /: arg_string)

let team_search : donor list service0 =
  EzAPI.service
    ~register:false
    ~params:[param_team_name]
    ~output:team_search
    ~error_outputs
    EzAPI.Path.(root // "teams")

(** Private API *)
let register_donor : (register_input, unit) post_service0 =
  EzAPI.post_service
    ~input:register_input
    ~output:Json_encoding.empty
    ~error_outputs
    EzAPI.Path.(root // "register_donor")

let donor_info : (string, (string * donor)) service1 =
  EzAPI.service
    ~output:dune_donor
    ~error_outputs
    EzAPI.Path.(root // "donor_info" /: arg_string)

(** Node API *)
let lookup_operation : (string, string) service1 =
  EzAPI.service
    ~register:false
    ~output:lookup_operation
    EzAPI.Path.(root // "chains" // "main" // "blocks" // "head" // "operation" /: arg_string)

let head_level : (int32 * int32) service0 =
  EzAPI.service
    ~register:false
    ~output:current_level
    EzAPI.Path.(root // "chains" // "main" // "blocks" // "head" // "helpers" // "current_level")

let constants : (int32 * string list) service0 =
  EzAPI.service
    ~register:false
    ~output:constants
    EzAPI.Path.(root // "chains" // "main" // "blocks" // "head" // "context" // "constants")

let rewards : (string, (int32 * int64) list) service1 =
  EzAPI.service
    ~register:false
    ~output:rewards
    EzAPI.Path.(root // "chains" // "main" // "blocks" // "head" // "context" // "delegates" /: arg_string // "frozen_balance_by_cycle")
