open MBytes

exception Binary_reading_error of string

type state = {b : MBytes.t; offset: int}

let (>>=) (o, s) f = let x, s = f s in (o, x), s

let uint8 s = get_int8 s.b s.offset, {s with offset = s.offset + 1}
let char s = get_char s.b s.offset, {s with offset = s.offset + 1}


let bool s = match char s with
  | '\000', s -> false, s
  | '\255', s -> true, s
  | c, _ -> raise (Binary_reading_error (Printf.sprintf "%c is not a boolean" c))

let opt read_value s = match char s with
  | '\000', s -> None, s
  | '\255', s -> let x, s = read_value s in Some x, s
  | c, _ -> raise (Binary_reading_error (Printf.sprintf "%c is not an option" c))

let int32 s = Int32.to_int (get_int32 s.b s.offset), {s with offset = s.offset + 4}

let uint64 ?(limit=10) s =
  let rec f acc i =
    if i > limit then raise (Binary_reading_error "int64 too long")
    else (
      let v = get_int8 s.b (s.offset + i) in
      let acc = Int64.(add (shift_left acc 7) (of_int @@ v land 0x7f)) in
      if v land 0x80 = 0 then acc, {s with offset = (s.offset + i + 1)}
      else f acc (i+1)) in
  f 0L 0

let uzarith ?(limit=10) s =
  let rec f acc i =
    if i > limit then raise (Binary_reading_error "unsigned zarith too long")
    else (
      let v = get_int8 s.b (s.offset + i) in
      let acc = Z.add (Z.shift_left acc 7) (Z.of_int @@ v land 0x7f) in
      if v land 0x80 = 0 then acc, {s with offset = s.offset + i + 1}
      else f acc (i+1)) in
  f Z.zero 0

let zarith ?(limit=10) s =
  let rec f acc i =
    if i > limit then raise (Binary_reading_error "signed zarith too long")
    else (
      let v = get_int8 s.b (s.offset + i) in
      let acc =
        if i = 0 then
          let aux = Z.add (Z.shift_left acc 6) (Z.of_int @@ v land 0x3f) in
          if v land 0x40 = 0 then Z.neg aux else aux
        else
          Z.add (Z.shift_left acc 7) (Z.of_int @@ v land 0x7f) in
      if v land 0x80 = 0 then acc, {s with offset = s.offset + i + 1}
      else f acc (i+1)) in
  f Z.zero 0

let elem f s =
  let len, s = int32 s in
  f len s

let list reader s =
  let f len s =
    let rec aux s l = function
      | 0 -> List.rev l, s
      | i when i > 0 -> let offset = s.offset in
        let x, s = reader s in
        aux s (x :: l) (i - (s.offset - offset))
      | _ -> raise (Binary_reading_error "list longer than expected") in
    aux s [] len in
  elem f s

let sub len s = MBytes.sub s.b s.offset len, {s with offset = s.offset + len}

let pkh s =
  let tag, s = uint8 s in
  let prefix = match tag with
    | 0 -> Crypto.Prefix.ed25519_public_key_hash
    | 1 -> Crypto.Prefix.secp256k1_public_key_hash
    | 2 -> Crypto.Prefix.p256_public_key_hash
    | n -> raise (Binary_reading_error (Printf.sprintf "wrong tag for pkh %d" n)) in
  let b, s = sub 20 s in
  Crypto.Pkh.b58enc ~prefix b, s

let contract s =
  let tag, s = uint8 s in
  match tag with
  | 0 -> pkh s
  | 1 -> let b, s = sub 20 s in
    Crypto.(Base58.simple_encode Prefix.contract_public_key_hash b), s
  | n -> raise (Binary_reading_error (Printf.sprintf "wrong tag for contract %d" n))

let pk s =
  let tag, s = uint8 s in
  let prefix, len = match tag with
    | 0 -> Crypto.Prefix.ed25519_public_key, 32
    | 1 -> Crypto.Prefix.secp256k1_public_key, 33
    | 2 -> Crypto.Prefix.p256_public_key, 33
    | n -> raise (Binary_reading_error (Printf.sprintf "wrong tag for pk %d" n)) in
  let b, s = sub len s in
  Crypto.Pkh.b58enc ~prefix b, s

let branch s =
  let b, s = sub 32 s in
  Crypto.(Base58.simple_encode Prefix.block_hash b), s

let signature s =
  let b, s = sub 64 s in
  Crypto.(Base58.simple_encode Prefix.generic_signature b), s

let script_expr_hash s =
  let b, s = sub 32 s in
  Crypto.(Base58.simple_encode Prefix.script_expr_hash b), s

let obj1 (a, _s) = a
let obj2 ((a, b), _s) = a, b
let obj3 (((a, b), c), _s) = a, b, c
let obj4 ((((a, b), c), d), _s) = a, b, c, d
let obj5 (((((a, b), c), d), e), _s) = a, b, c, d, e
let obj6 ((((((a, b), c), d), e), f), _s) = a, b, c, d, e, f
let obj7 (((((((a, b), c), d), e), f), g), _s) = a, b, c, d, e, f, g
let obj8 ((((((((a, b), c), d), e), f), g), h), _s) = a, b, c, d, e, f, g, h
let obj9 (((((((((a, b), c), d), e), f), g), h), i), _s) = a, b, c, d, e, f, g, h, i
let obj10 ((((((((((a, b), c), d), e), f), g), h), i), j), _s) = a, b, c, d, e, f, g, h, i, j

let start b = {b; offset = 0}
