(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2019 Origin Labs - contact@origin-labs.com                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

exception Forge_error of string

let int16 _ = 2
let int32 _ = 4
let int64 _ = 8
let char _ = 1
let bool _ = 1
let list l = List.fold_left (+) 0 l
let chars l = List.length l
let opt f x = 1 + f x
let n_zarith z =
  let bits = Z.numbits z in
  if bits = 0 then 1 else (bits + 6) / 7
let n_int64 i = n_zarith (Z.of_int64 i)
let zarith z = (Z.numbits z + 1 + 6) / 7
let elem x = 4 + x

let pkh _ = 21
let contract _ = 22
let source_manager _ = 21
let pk s =
  if String.length s < 4 then
    raise @@ Forge_error (Printf.sprintf "wrong format for pk: %S" s)
  else match String.sub s 0 4 with
    | "edpk" -> 33
    | _ -> 34

let branch _ = 32
let signature _ = 64
let script_expr_hash _ = 32
