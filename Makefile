PROJECT_NAME:=dune-folding
DATABASE:=dune_folding
WEBHOST:=http://localhost:8888
NODE:=http://localhost:8734
RLS_DIR:=www

-include Makefile.config

all: build website

build: project-db-update
	PGDATABASE=$(DATABASE) dune build
	@mkdir -p bin
	@cp -f _build/default/src/api/api_server.exe bin/api-server
	@cp -f _build/default/src/api/rewards.exe bin/reward-distributor
	@cp -f _build/default/src/api/print_distribution.exe bin/print-distribution

website:
	@mkdir -p www
	@cp -f _build/default/src/ui/main_ui.bc.js www/$(PROJECT_NAME)-ui.js
	@rsync -ar static/* www

submodule:
	@git submodule init
	@git submodule update

release:
	sudo cp -r www/* $(RLS_DIR)

clean:
	@dune clean

install:
	@dune install

project-db-updater:
	PGDATABASE=$(DATABASE) dune build src/db/update

project-db-update: project-db-updater
	@$(MAKE) db-update

build-deps:
	@opam install --deps-only .

DBUPDATER=_build/default/src/db/update/db_updater.exe
DBWITNESS=--witness db-version.txt
DBNAME=--database $(DATABASE)
-include libs/ez-pgocaml/libs/ez-pgocaml/Makefile.ezpg
