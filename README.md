# Dune Folding



## Building

First, pgocaml requires to disable opam sandoxing:
https://camlspotter.gitlab.io/blog/2019-04-16-pgocaml/

Then:
```
make submodule
make build-deps
make
```

## Launching
```
cd www && php -S localhost:8888
```
```
./api-server -p 8080
```
```
bin/reward-distributor --baker <dn1...> --reward-percent 100 --cycles 2 <edsk...>
```
